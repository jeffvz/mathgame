package com.example.mathgame

import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.navigation.findNavController


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [main_window.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [main_window.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class main_window : Fragment() {
    // TODO: Rename and change types of parameters
    internal lateinit var addButton: Button
    internal lateinit var subButton:Button
    internal lateinit var multButton:Button
    internal lateinit var divButton:Button

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        //Segun el ciclo de vida de un fragment, en este funcion se termina de crear todo.
        addButton = view.findViewById(R.id.addButton)
        addButton.setOnClickListener { goToMain("+") }
        subButton = view.findViewById(R.id.subButton)
        subButton.setOnClickListener{goToMain("-")}
        multButton = view.findViewById(R.id.multButton)
        multButton.setOnClickListener { goToMain("*") }
        divButton = view.findViewById(R.id.divButton)
        divButton.setOnClickListener { goToMain("/") }

    }

    fun goToMain(operation:String){
        val action = main_windowDirections.actionMainWindowToOperationWindow(operation) //Cuando se usa Navigation Editor se crea una clase automaticamente (WelcomeFragmentDirections) con las acciones del fragment (se crea funciones por cada accion).
        view?.findNavController()?.navigate(action) //Se recupera la instancia que controla el navegador.
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

    }
}

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main_window, container, false)
    }


}
